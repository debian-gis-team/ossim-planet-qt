Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: OSSIM Planet Qt
Upstream-Contact: OSSIM Developers <ossim-developer@lists.sourceforge.net>
Source: https://download.osgeo.org/ossim/source/
 The upstream tarball is repacked to exclude all subdirectories other than
 those for ossimPlanetQt.
Files-Excluded: csmApi/*
 csm_plugins/*
 gsoc/*
 libwms/*
 oms/*
 ossim/*
 ossimGui/*
 ossimPlanet/*
 ossimPredator/*
 ossim_package_support/*
 ossim_plugins/*
 ossim_qt4/*
 ossimjni/*
 planet_message/*
 pqe/*

Files: ossimPlanetQt/*
Copyright: 2003, Neil Salter
License: LGPL-2.1+

Files: ossimPlanetQt/include/ossimPlanetQt/ossimPlanetQtExport.h
Copyright: 2004, Garrett Potts
License: WMSGPL

Files: debian/*
Copyright: 2016, Massimo Di Stefano <epiesasha@me.com>
License: GPL-3+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public License
 version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.

License: WMSGPL
                 libwms Public License, Version 0.0
                 ==========================================
 .
   Copyright (C) 2004 Garrett Potts.
 .
   Everyone is permitted to copy and distribute verbatim copies
   of this licence document, but changing it is not allowed.
 .
                        LIBWMS PUBLIC LICENCE
      TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 .
   This library is free software; you can redistribute it and/or modify it
   under the terms of the libwms Public License (WMSGPL) version 0.0
   or later.
 .
   Notes: the OSGPL is based on the LGPL, with the 4 exceptions laid in
   the wxWindows section below.  The LGPL in the final section of this
   license.
 .
 .
 -------------------------------------------------------------------------------
 .
                 wxWindows Library Licence, Version 3
                 ====================================
 .
   Copyright (C) 1998 Julian Smart, Robert Roebling [, ...]
 .
   Everyone is permitted to copy and distribute verbatim copies
   of this licence document, but changing it is not allowed.
 .
                        WXWINDOWS LIBRARY LICENCE
      TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 .
   This library is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public Licence as published by
   the Free Software Foundation; either version 2 of the Licence, or (at
   your option) any later version.
 .
   This library is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library
   General Public Licence for more details.
 .
   You should have received a copy of the GNU Library General Public Licence
   along with this software, usually in a file named COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 51 Franklin Street,
   Fifth Floor, Boston, MA 02110-1301 USA.
 .
   EXCEPTION NOTICE
 .
   1. As a special exception, the copyright holders of this library give
   permission for additional uses of the text contained in this release of
   the library as licenced under the wxWindows Library Licence, applying
   either version 3 of the Licence, or (at your option) any later version of
   the Licence as published by the copyright holders of version 3 of the
   Licence document.
 .
   2. The exception is that you may use, copy, link, modify and distribute
   under the user's own terms, binary object code versions of works based
   on the Library.
 .
   3. If you copy code from files distributed under the terms of the GNU
   General Public Licence or the GNU Library General Public Licence into a
   copy of this library, as this licence permits, the exception does not
   apply to the code that you add in this way.  To avoid misleading anyone as
   to the status of such modified files, you must delete this exception
   notice from such code and/or adjust the licensing conditions notice
   accordingly.
 .
   4. If you write modifications of your own for this library, it is your
   choice whether to permit this exception to apply to your modifications.
   If you do not wish that, you must delete the exception notice from such
   code and/or adjust the licensing conditions notice accordingly.
 .
 .
 ------------------------------------------------------------------------------

